#pragma once
#include <array>

extern std::array<int, 5> GlobalArray ;

void printGlobalArray();

std::array<int, 5> getGlobalArray();

