#include "global_static.hh"
#include <iostream>
#include <numeric>

std::array<int,5> GlobalArray= []()
{
	decltype (GlobalArray) arr;
	std::iota(arr.begin(), arr.end(), 2);
	return arr;
}();

//std::iota(GlobalArray.begin(), GlobalArray.end(), 1);

void printGlobalArray()
{
	for(auto i: GlobalArray) std::cout << i << " ";
	std::cout << std::endl;
}

std::array<int, 5> getGlobalArray()
{
	return decltype (GlobalArray)(GlobalArray);
}
