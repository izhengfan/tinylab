#include "core_utils.hh"
#include <chrono>
using namespace std;

int UniqueId::get()
{
	id++;
	return id;
}

double SecSinceEpoch()
{
	auto now = chrono::system_clock::now();
	return chrono::duration_cast<chrono::microseconds>(
	            now.time_since_epoch()).count() / 1e6;
}
