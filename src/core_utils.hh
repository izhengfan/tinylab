#pragma once
// delete raw pointer and set it null
template <typename PTR_T>
inline void delete_it_null(PTR_T &p)
{
	delete p;
	p = nullptr;
}

double SecSinceEpoch();

/**
 * Get a unique id (int) each time, in cases when you need to assign many
 * ids, which must be different to each other
 */
class UniqueId
{
public:
	/**
	 * To use get(), an instance of UniqueId is required;
	 * different instances may generate same id.
	 * If you need global uniqueness, you can declare a global static UniqueId
	 */
	int get();

private:
	int id {0};
};

