#pragma once
#include <memory>
#include <queue>
#include <functional>
#include <iterator>
#include <vector>


// PriorityQueue copied from https://www.redblobgames.com/pathfinding/a-star/implementation.html#cplusplus
template<typename T, typename PRIORITY_T>
struct PriorityQueue
{
	typedef std::pair<PRIORITY_T, T> PQElement;
	std::priority_queue<PQElement, std::vector<PQElement>, std::greater<PQElement>> elements;

	inline bool empty() const
	{ return elements.empty(); }

	inline void put(T item, PRIORITY_T priority)
	{ elements.emplace(priority, item); }

	T get()
	{
		T best_item = elements.top().second;
		elements.pop();
		return best_item;
	}
};

/**
 * Hash for two elements combined, such as std::pair.
 * Useful in cases like std::unordered_set and std::unordered_map.
 * @note std::hash<T1> and std::hash<T2> should have been defined
 */
template<typename T1, typename T2>
struct PairHash
{
	static std::size_t calc(const T1& first, const T2& second)
	{
		std::size_t h1 = std::hash<T1>{}(first);
		std::size_t h2 = std::hash<T2>{}(second);
		return h1 ^ (h2 << 1);
	}

	/**
	 * Convenient if using std::pair
	 * @example unordered_set<pair<int,int>, PairHash<int,int>>;
	 */
	std::size_t operator() (const std::pair<T1,T2>& p) const
	{
		return calc(p.first, p.second);
	}
};

/**
 * Comparor for std::weak_ptr, using the < operator of std::shared_ptr
 * Usage: std::set<std::weak_ptr<int>, WPtrCompare<int>> some_set;
 */
template<typename T>
struct WPtrCompare
{
	using WPtr = std::weak_ptr<T>;
	bool operator() (const WPtr& lhs, const WPtr& rhs) const
	{
		auto plhs = lhs.lock();
		if(!plhs) return true;
		auto prhs = rhs.lock();
		if(!prhs) return false;

		return plhs < prhs;
	}
};
/**
 * Remove the heading num elements of a container
 */
template<typename ContainerT>
void eraseHead(ContainerT& container, std::size_t num)
{
	auto itr = container.begin();
	std::advance(itr, static_cast<std::ptrdiff_t>(num));
	container.erase(container.begin(), itr);
}
