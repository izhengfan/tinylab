#include "id.hh"
#include "core_utils.hh"
#include <gtest/gtest.h>
TEST(ID_TEST, COPY)
{
	ID id10 {1, "haha"};
	ID id20 = id10;
	EXPECT_EQ(strcmp(id20.name, "haha"), 0);

}

TEST(ID_TEST, UNIQUE)
{
	static UniqueId unik_id;
	for(int i = 0; i < 4; i++)
	{
		int b = unik_id.get();
		static int a = unik_id.get();
		EXPECT_EQ(a, 2);
		if(i==0) EXPECT_EQ(b, 1);
		else EXPECT_EQ(b, i+2);
	}

}
