#include <deque>
#include <gtest/gtest.h>

TEST(DEQUE, ALL)
{
    std::deque<int> dq{1,2,3};
    EXPECT_EQ(dq[0], 1);
    EXPECT_EQ(dq[1], 2);
    EXPECT_EQ(dq[2], 3);
	dq.pop_front();
    EXPECT_EQ(dq[0], 2);
    EXPECT_EQ(dq[1], 3);
	dq.push_back(4);
    EXPECT_EQ(dq[2], 4);
	dq.push_front(1);
    EXPECT_EQ(dq[0], 1);
    EXPECT_EQ(dq[1], 2);
    EXPECT_EQ(dq[2], 3);
    EXPECT_EQ(dq[3], 4);
}