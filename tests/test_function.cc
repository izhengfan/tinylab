#include <gtest/gtest.h>
#include <vector>
#include <functional>
#include <type_traits>

using namespace std;

template <typename T>
class TempStruct
{
public:
	using T_IN = const T&;
	function<bool(T_IN)> fcr = [](T_IN t) { return t.copied; };

	using T_VAL = typename remove_const<typename remove_reference<T_IN>::type>::type;
	function<bool(T_VAL)> fv = [](T_VAL t) { return t.copied; };
};

class Foo
{
public:
	Foo() = default;
	Foo(const Foo&) { copied = true; }
	bool copied {false};
};

TEST(STD_FUNCTION, FUNCTION_TEMP)
{
	Foo f;
	TempStruct<Foo> tf;
	EXPECT_TRUE(tf.fv(f));
	EXPECT_FALSE(tf.fcr(f));

	// let pass-by-value equal to pass-by-cref, it is still pass-by-value
	tf.fv = tf.fcr;
	EXPECT_TRUE(tf.fv(f));
	// let pass-by-cref equal to pass-by-value, it is now pass-by-value
	tf.fcr = tf.fv;
	EXPECT_TRUE(tf.fcr(f));
}
