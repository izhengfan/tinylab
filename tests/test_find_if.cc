#include <gtest/gtest.h>
#include <numeric>
#include <cmath>
#include <algorithm>
#include <vector>
#include <iterator>
using namespace std;

TEST(FIND_IF, VECTOR)
{
	std::vector<int> vi { 1, 2, 3, 4, 5 };
	auto itr = std::find_if(vi.begin(), vi.end(), [](auto i){ return i==2;});
	EXPECT_FALSE(itr == vi.end());
	auto nit = next(itr);
	EXPECT_TRUE(*itr == 2);
	EXPECT_TRUE(*nit == 3);
	EXPECT_TRUE(std::find_if(nit, vi.end(), [](auto i){ return i==2; }) == vi.end());
	vi.push_back(2);
	EXPECT_TRUE(*nit == 3);
	EXPECT_FALSE(std::find_if(nit, vi.end(), [](auto i){ return i==2; }) == vi.end());
}
