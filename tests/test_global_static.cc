#include "global_static.hh"
#include <gtest/gtest.h>

TEST(GLOBAL_STATIC, ALL)
{
	for(int i =0; i < 5; i++)
		EXPECT_EQ(GlobalArray[i], 2+i);
	for(int i =0; i < 5; i++) GlobalArray[i] = i*3;
	auto arr = getGlobalArray();
	for(int i =0; i < 5; i++)
		EXPECT_EQ(arr[i], i*3);

	decltype (GlobalArray) arr3 {1,2,3};
	EXPECT_EQ(arr3[0], 1);
	EXPECT_EQ(arr3[2], 3);
	EXPECT_EQ(arr3[4], 0);
}
