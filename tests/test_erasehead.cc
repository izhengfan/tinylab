#include <gtest/gtest.h>
#include "stl_utils.hh"
#include <set>
#include <list>

using namespace std;

TEST(ERASEHEAD, VECTOR)
{
	vector<int> vi {1,3,4,5,6};
	auto vi2 = vi;
	eraseHead(vi, 0);
	EXPECT_EQ(vi, vi2);

	eraseHead(vi, 1);
	EXPECT_EQ(vi, vector<int>({3,4,5,6}));

	eraseHead(vi, vi.size());
	EXPECT_TRUE(vi.empty());
}

TEST(ERASEHEAD, SET)
{
	set<int> si {1,2,3,4,5};
	eraseHead(si, 3);
	EXPECT_EQ(si.size(), 2);
	eraseHead(si, si.size());
	EXPECT_FALSE(si.size());
}
TEST(ERASEHEAD, MAP)
{

	map<int,int> mi { {1,2}, {2,3}, {5,6}, {78,8} };
	eraseHead(mi, 2);
	EXPECT_EQ(mi.begin()->first, 5);
	EXPECT_EQ(mi.begin()->second, 6);
	EXPECT_EQ(mi.rbegin()->first, 78);
	EXPECT_EQ(mi.rbegin()->second, 8);
}

TEST(ERASEHEAD, LIST)
{
	list<int> li {1,3,4,5,6};
	auto li2 = li;
	eraseHead(li, 0);
	EXPECT_EQ(li, li2);

	eraseHead(li, 1);
	EXPECT_EQ(li, list<int>({3,4,5,6}));

	eraseHead(li, li.size());
	EXPECT_TRUE(li.empty());
}

