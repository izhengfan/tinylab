#include <gtest/gtest.h>
#include <vector>

TEST(STL_VECTOR, ReserveCapacity)
{
	std::vector<int> v;
	v.reserve(1000);
	EXPECT_EQ(v.size(), 0);
	EXPECT_EQ(v.capacity(), 1000);
	v.resize(1);
	EXPECT_EQ(v.capacity(), 1000);
	v.resize(200);
	EXPECT_EQ(v.capacity(), 1000);
}

TEST(STL_VECTOR, Swap)
{
	std::vector<int> v1;
	EXPECT_EQ(v1.size(), 0);
	std::vector<int> v2 = { 1,2, 3};
	v1.swap(v2);
	EXPECT_EQ(v2.size(), 0);
	EXPECT_EQ(v1.size(), 3);
}


