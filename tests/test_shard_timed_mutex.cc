#include <memory>
#include <mutex>
#include <thread>
#include <shared_mutex>
#include <gtest/gtest.h>
using namespace std;

class TestShardTimedMutex
{
public:
	TestShardTimedMutex () = default;
	~TestShardTimedMutex () = default;

	std::shared_timed_mutex mut;
	std::string data ;
};


TEST(SHARED_TIMED_MUTEX, Once)
{
	string write_data = "Writing TestShardTimedMutex";
	string old_data{"TestShardTimedMutex"};
	std::string str1, str2;

	TestShardTimedMutex data;
	data.data = old_data;
	thread writer1 ([&data, &write_data](){
		unique_lock<shared_timed_mutex> lock(data.mut);
		data.data = write_data;
		this_thread::sleep_for(chrono::milliseconds(30));
		lock.unlock();
		this_thread::sleep_for(chrono::milliseconds(30));
		});
	thread reader1 ([&data,&str1](){
		this_thread::sleep_for(chrono::milliseconds(10));
		shared_lock<shared_timed_mutex> lock(data.mut);
		str1 = data.data;
		this_thread::sleep_for(chrono::milliseconds(60));
		});
	thread reader2 ([&data,&str2](){
		this_thread::sleep_for(chrono::milliseconds(10));
		shared_lock<shared_timed_mutex> lock(data.mut);
		str2 = data.data;
		this_thread::sleep_for(chrono::milliseconds(80));
		lock.unlock();
		str2 = "";
		this_thread::sleep_for(chrono::milliseconds(30));
		});

	this_thread::sleep_for(chrono::milliseconds(20));

	EXPECT_EQ(str1, string());
	EXPECT_EQ(str2, string());

	this_thread::sleep_for(chrono::milliseconds(30));
	EXPECT_EQ(str1, write_data);
	EXPECT_EQ(str2, write_data);


	if(writer1.joinable()) writer1.join();
	if(reader1.joinable()) reader1.join();

	thread writer2 ([&data, &old_data](){
		unique_lock<shared_timed_mutex> lock(data.mut);
		data.data = old_data;
		});
	this_thread::sleep_for(chrono::milliseconds(10));
	EXPECT_EQ(data.data, write_data);

	this_thread::sleep_for(chrono::milliseconds(60));
	EXPECT_EQ(data.data, old_data);
	EXPECT_EQ(str2, string());


	if(reader2.joinable()) reader2.join();
	if(writer2.joinable()) writer2.join();
}

