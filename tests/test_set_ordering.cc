#include <set>
#include <gtest/gtest.h>

struct MyPair
{
	int a;
	int b;
	bool operator <  (const MyPair& that) const
	{
		return b  < that.b;
	}
};

TEST(STL_SET, Ordering)
{
	std::set<int> si;
	for(int i = 0; i < 8; i++) si.insert(i);
	EXPECT_EQ(*si.begin(), 0);
	EXPECT_EQ(*si.rbegin(), 7);
	MyPair a {3,2};
	MyPair b {2,3};
	std::set<MyPair> s;
	s.insert(a);
	s.insert(b);
	EXPECT_EQ(s.begin()->a, 3);
	EXPECT_EQ(s.rbegin()->a, 2);
	b.b = -10;
	EXPECT_EQ(s.begin()->a, 3);
	EXPECT_EQ(s.rbegin()->a, 2);

}
