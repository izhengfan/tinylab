#include <mutex>
#include <thread>
#include <atomic>
#include <gtest/gtest.h>

TEST(UNIQUE_LOCK, UNLOCK)
{
	std::atomic_int ai {1};
	std::mutex mut;
	EXPECT_EQ(ai, 1);
	std::thread func2([&ai, &mut](){
		std::unique_lock<std::mutex> locker(mut);
		ai = 2;
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
	});
	std::thread func3([&ai, &mut](){
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		std::unique_lock<std::mutex> locker(mut);
		ai = 3;
	});
	std::this_thread::sleep_for(std::chrono::milliseconds(150));
	EXPECT_EQ(ai, 2);
	if(func2.joinable()) func2.join();
	if(func3.joinable()) func3.join();

	std::thread func4([&ai, &mut](){
		std::unique_lock<std::mutex> locker(mut);
		ai = 4;
		locker.unlock();
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
	});
	std::thread func5([&ai, &mut](){
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		std::unique_lock<std::mutex> locker(mut);
		ai = 5;
	});
	std::this_thread::sleep_for(std::chrono::milliseconds(150));
	EXPECT_EQ(ai, 5);
	if(func4.joinable()) func4.join();
	if(func5.joinable()) func5.join();
}

TEST(UNIQUE_LOCK, TryLock)
{
	std::atomic_int ai {1};
	std::timed_mutex mut;
	EXPECT_EQ(ai, 1);
	std::thread func2([&ai, &mut](){
		std::unique_lock<std::timed_mutex> locker(mut, std::defer_lock);
		locker.try_lock();
		ai = 2;
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
	});
	std::thread func3([&ai, &mut](){
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		std::unique_lock<std::timed_mutex> locker(mut, std::defer_lock);
		ai = 3;
	});
	std::this_thread::sleep_for(std::chrono::milliseconds(150));
	EXPECT_EQ(ai, 3);
	if(func2.joinable()) func2.join();
	if(func3.joinable()) func3.join();

	std::thread func4([&ai, &mut](){
		std::unique_lock<std::timed_mutex> locker(mut);
		ai = 4;
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
	});
	std::thread func5([&ai, &mut](){
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		std::unique_lock<std::timed_mutex> locker(mut, std::chrono::milliseconds(20));
		ai = 5 + locker.owns_lock();
	});
	std::this_thread::sleep_for(std::chrono::milliseconds(240));
	EXPECT_EQ(ai, 5);
	if(func4.joinable()) func4.join();
	if(func5.joinable()) func5.join();

	std::thread func6([&ai, &mut](){
		std::unique_lock<std::timed_mutex> locker(mut);
		ai = 6;
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
	});
	std::thread func7([&ai, &mut](){
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		std::unique_lock<std::timed_mutex> locker(mut, std::chrono::milliseconds(120));
		ai = 7 + locker.owns_lock();
	});
	std::this_thread::sleep_for(std::chrono::milliseconds(240));
	EXPECT_EQ(ai, 8);
	if(func6.joinable()) func6.join();
	if(func7.joinable()) func7.join();
}
