#include <gtest/gtest.h>

enum  Type: int
{
	A = 1,
	B = 2,
	C = 4,
	D = 8
};

enum TaskType : uint8_t
{
	MOVE_TO = 1 << 0,
	PUT     = 1 << 1,
	GET     = 1 << 2,
	NONE    = 1 << 7
};

TEST(ENUM, OR)
{
	EXPECT_TRUE(Type::B & (Type::A | Type::B));
	EXPECT_TRUE(Type::A & (Type::A | Type::B));
	EXPECT_TRUE(Type::B & (Type::C | Type::B));
	EXPECT_FALSE(Type::D & (Type::A | Type::B));

	EXPECT_TRUE(MOVE_TO & (MOVE_TO | GET | PUT));
	EXPECT_TRUE(PUT & (PUT | GET));
	EXPECT_EQ(MOVE_TO, 1);
	EXPECT_EQ(PUT, 2);
	EXPECT_EQ(GET, 4);
	EXPECT_EQ(NONE, 128);
}
