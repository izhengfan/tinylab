#include <msgpack.hpp>
#include <gtest/gtest.h>
#include <fstream>

struct My {
    static char ca[10];
    int a;
    std::string s;
    double d;
    std::vector<float> vf;
    MSGPACK_DEFINE(ca, a, s, d, vf);
};
char My::ca[10] { "MyMyMyMyM"};



TEST(MSGPACK, BASIC)
{
    My my{1, "aa", 9.9, {3.14f, 2.13f}};
    msgpack::sbuffer sbuf;
    msgpack::pack(sbuf, my);
    msgpack::object_handle oh =
            msgpack::unpack(sbuf.data(), sbuf.size());

    msgpack::object obj = oh.get();

    My rvec;
    obj.convert(rvec);


    EXPECT_EQ(rvec.a, my.a);
    EXPECT_EQ(rvec.s, my.s);
    EXPECT_EQ(rvec.d, my.d);
    EXPECT_EQ(rvec.vf, my.vf);
}

TEST(MSGPACK, ReadWriteBin)
{
    std::string filename = std::string(tinylab_PROJECT_BIN_DIR) + "/test.bin";
    std::fstream s(filename, s.binary | s.out);
    if (!s.is_open()) {
        std::cout << "failed to open " << filename << '\n';
    }
    else
    {
        My my{1, "aa", 9.9, {3.14f, 2.13f}};
        msgpack::sbuffer sbuf;
        msgpack::pack(sbuf, my);
        s.write(sbuf.data(), sbuf.size());
        my.a = 2;
        my.s = "bbb";
        my.vf.clear();

        // write
        //double d = 3.14;
        //s.write(reinterpret_cast<char*>(&d), sizeof d); // binary output
        //s << 123 << "abc";                              // text output

        s.close();
        s.open(filename, s.binary | s.in);
        // for fstream, this moves the file position pointer (both put and get)
        //s.seekp(0);

        // read
        //s.read(reinterpret_cast<char*>(&d), sizeof d); // binary input
        //int n;
        //std::string str;
        //if (s >> n >> str)                             // text input
            //std::cout << "read back from file: " << d << ' ' << n << ' ' << str << '\n';
        std::cout << std::endl;
    }
}
