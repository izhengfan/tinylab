#include <iostream>
#include <gtest/gtest.h>
#include <unordered_set>
#include "core_utils.hh"

using namespace std;
TEST(DELETE_AND_NULL, DELETE_AND_NULL)
{
	std::unordered_set<int> si;
	for(int i = 0; i < 18; i++)
		si.insert(i);
	auto p1  = new int(1);
	delete_it_null(p1);
	EXPECT_EQ(p1, nullptr);
}

