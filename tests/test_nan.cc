#include <gtest/gtest.h>
#include <numeric>
#include <cmath>
TEST(TEST_NAN, IS_NAN)
{
	double a = NAN;
	double b = a;
	EXPECT_TRUE(std::isnan(b));
}
