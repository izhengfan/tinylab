#include <memory>
#include <gtest/gtest.h>

using std::shared_ptr;
class Base
{
public:
	using Ptr = shared_ptr<Base>;
	int a;
	int b;
};

class Inherit: public Base
{
public:
	using Ptr = shared_ptr<Inherit>;
	int c;

	static Base::Ptr create(int ain, int bin)
	{
		Ptr ret = Ptr(new Inherit);
		ret->a = ain;
		ret->b = bin;
		ret->c = 99;
		return std::static_pointer_cast<Base>(ret);
	}
};

using Creator = std::function<Base::Ptr(int, int)>;
Base::Ptr create(int ain, int bin, Creator creator=nullptr)
{
	if(creator)
		return creator(ain, bin);
	else
		return Base::Ptr(new Base{ain, bin});
}

TEST(CAST_CREATOR, ALL)
{
	auto base = create(2, 3, Inherit::create);
	EXPECT_EQ(base->a, 2);
	EXPECT_EQ(base->b, 3);
	Inherit::Ptr inhe = std::static_pointer_cast<Inherit>(base);
	EXPECT_EQ(inhe->c, 99);
}
