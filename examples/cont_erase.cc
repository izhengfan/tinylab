#include <iostream>
#include <vector>
#include <map>
#include <set>
#include "stl_utils.hh"
using namespace std;

int main(int argc, char *argv[])
{
	vector<int> vi {1,2,3,3,4};
	eraseHead(vi, 2);
	for(auto&& i: vi) cout << i << " ";
	cout << endl;
	for(int i = 0; i < vi.size(); ++i)
	{
		cout << vi[i] << endl;
	}

	set<int> si {1,2,3,3,4};
	eraseHead(si, 2);
	for(auto&& i: si) cout << i << " ";
	cout << endl;

	map<int,int> mi { {1,3}, {2,3}, {4,5}, {0,2} };
	eraseHead(mi, 2);
	for(auto&& i: mi) cout << i.first << " " << i.second << "; ";
	cout << endl;
}

