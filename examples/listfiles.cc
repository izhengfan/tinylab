#include <dirent.h>
#include <stdio.h>
#include <string>
#include <sys/stat.h>
int main(int argc, char *argv[])
{
	DIR *dpdf;
	struct dirent *epdf;
	struct stat st;

	dpdf = opendir(argv[1]);
	if (dpdf != NULL)
	{
		while (epdf = readdir(dpdf))
		{
			std::string name(epdf->d_name);
			std::string full_file_name = std::string(argv[1]) + "/" + name;
			if(name == "." || name == "..")
				continue;
			if (stat(full_file_name.c_str(), &st) == -1)
				continue;
			const bool is_directory = (st.st_mode & S_IFDIR) != 0;
			if (is_directory)
				continue;
			printf("Filename: %s\n",epdf->d_name);
			// std::cout << epdf->d_name << std::endl;
		}
	}
	closedir(dpdf);
	return 0;
}

