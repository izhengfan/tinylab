#include <iostream>
#include <set>
#include "stl_utils.hh"

using namespace std;

int main(int argc, char *argv[])
{
	set<weak_ptr<int>, WPtrCompare<int>> spi;
	auto p1 = make_shared<int> (1);
	auto p2 = make_shared<int> (2);
	auto p3 = make_shared<int> (3);
	spi.insert(p1);
	spi.insert(p2);
	spi.insert(p3);
	weak_ptr<int> wp1 = p1;
	weak_ptr<int> wp2 = p2;
	weak_ptr<int> wp3 = p3;
	for(auto&& pi: spi)
		cout << *(pi.lock()) << endl;
	cout << endl;
	if(spi.count(wp1))
		cout << "spi has wp1" << endl;
	if(spi.count(wp2))
		cout << "spi has wp2" << endl;
	if(spi.count(wp3))
		cout << "spi has wp3" << endl;
	weak_ptr<int> wp0;
	if(!spi.count(wp0))
		cout << "spi has not wp0" << endl;
	auto p4 = make_shared<int> (1);
	weak_ptr<int> wp4 = p4;
	if(!spi.count(wp4))
		cout << "spi has not wp4" << endl;

	if(spi.count(p1))
		cout << "spi has p1" << endl;
	if(spi.count(weak_ptr<int>(p1)))
		cout << "spi has w<p1>" << endl;

	return 0;
}

